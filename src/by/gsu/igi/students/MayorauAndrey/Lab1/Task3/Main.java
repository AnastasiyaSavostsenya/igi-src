package by.gsu.igi.students.MayorauAndrey.Lab1.Task3;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.print("Радиус = ");
        double radius = scanner.nextByte();

        double perimeter = 2 * Math.PI * radius;
        System.out.println("Периметр = " + perimeter);

        double area = Math.PI * Math.pow(radius, 2);
        System.out.println("Площадь = " + area);
    }
}
